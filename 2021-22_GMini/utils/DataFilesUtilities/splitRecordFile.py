#!/usr/local/bin/python3.7
# !/usr/bin/env python3

""" Script to split GMini data file in 1 file per source,
    and modify file as follows:
        - Add header line
        - Add column CorrectedTS in subs data file
        - Add column relativeTS in all files
        - Add and fill column relativeAltitude in all files

    To run the script at command prompt:
        make sure the directory containing the script is in PYTHON_PATH
        cd to directory containing data file
        python3.7 <thisScript> InputFile

    THIS SCRIPT IS OBSOLETE AND REPLACED BY processRecordFile.py
"""
# import sys  # for argv
import os.path
import csv
import sys
from typing import List

MYSELF = os.path.basename(sys.argv[0])
TITLE = '\nG-Mini record file split utility\n' \
          '--------------------------------'
USAGE = 'Usage: ' + MYSELF + ' <fileToSplitPossiblyWithoutExtension>'
NUM_RECORD_FIELDS = 14
TIMESTAMP_FIELD = 0
ALTITUDE_FIELD = 7
REF_ALTITUDE_FIELD = 8
THERM1_TEMP_INDEX = 10
SOURCE_ID_FIELD = 12  # idx of source ID field
GPS_ALTITUDE_FIELD = 4  # idx of source ID field
FORCE_OUTPUT_FILE_OVERWRITE = False  # for development only.
HEADER_MAIN = 'timestamp,relTS,GPS_Measures,GPS_LatDegrees,GPS_LongDegrees,GPS_Alt,tempBMP,pressure,altitude,' \
              'ref.altitude,rel.altitude,descVelocity,tempThermis1,tempThermist2,sourceID,subCanEjected'
HEADER_SUB = 'timestamp,corrTS, relTS,GPS_Measures,GPS_LatDegrees,GPS_LongDegrees,GPS_Alt,tempBMP,pressure,altitude,' \
             'ref.altitude,rel.altitude,descVelocity,tempThermis1,tempThermist2,sourceID,subCanEjected'

print(TITLE)
print()

print('*** WARNING: THIS UTILITY IS OBSOLETE. Consider using processRecordFile.py instead.')
answer = input('Do you want to proceed (y/n)?')
if (answer is not 'y') and (answer is not 'Y'):
    print('Aborted.')
    exit(99)

if len(sys.argv) != 2:
    print(MYSELF, ': Missing or extraneous argument (Aborted)')
    print(USAGE)
    sys.exit(1)

inputFileName = sys.argv[1]
fileFound = False
if os.path.exists(inputFileName):
    fileFound = True
else:
    inputFileName = sys.argv[1] + ".csv"
    if os.path.exists(inputFileName):
        fileFound = True
    else:
        inputFileName = sys.argv[1] + ".CSV"
        if os.path.exists(inputFileName):
            fileFound = True
        else:
            inputFileName = sys.argv[1] + ".txt"
            if os.path.exists(inputFileName):
                fileFound = True
            else:
                inputFileName = sys.argv[1] + ".TXT"
                if os.path.exists(inputFileName):
                    fileFound = True
if not fileFound:
    print(MYSELF, ': cannot find input file \'' + sys.argv[1] + '\' (Aborted).')
    print(USAGE)
    sys.exit(2)

print('Processing file \'' + inputFileName + '\'')

fileNameNoExt = os.path.splitext(inputFileName)[0]
outputFileNames: List[str] = [fileNameNoExt + '_0.csv', fileNameNoExt + '_1.csv', fileNameNoExt + '_2.csv']
for name in outputFileNames:
    if (not FORCE_OUTPUT_FILE_OVERWRITE) and os.path.exists(name):
        print(MYSELF + ': ERROR: Output file \'' + name + '\' exists (Aborted)')
        exit(3)

inputFile = open(inputFileName, 'r')
reader = csv.reader(inputFile, delimiter=',')
outputFiles = []
for i in [0, 1, 2]:
    outputFiles.append(open(outputFileNames[i], 'w'))
    if i == 0:
        outputFiles[i].write(HEADER_MAIN + '\n')
    else:
        outputFiles[i].write(HEADER_SUB + '\n')

numSkippedRows = 0
numRecords = 0
numRecordsPerSource = [0, 0, 0]
minTS: List[int] = [1E20, 1E20, 1E20]
maxTS = [0, 0, 0]
maxRelativeAltitude = [-1000.0, -1000.0, -1000.0]
maxRelativeAltitudeTS = [0, 0, 0]
maxTemperature = [-1000.0, -1000.0, -1000.0]
maxTemperatureTS = [0, 0, 0]
minTemperature = [1000.0, 1000.0, 1000.0]
minTemperatureTS = [0, 0, 0]

for row in reader:
    if (len(row) != NUM_RECORD_FIELDS) or (not row[0].isdigit()) or (not row[SOURCE_ID_FIELD].isdigit()):
        numSkippedRows += 1
        continue
    else:
        sourceID = int(row[SOURCE_ID_FIELD])
        numRecords += 1
        numRecordsPerSource[sourceID] += 1
        ts = int(row[0])
        if ts > maxTS[sourceID]:
            maxTS[sourceID] = ts
        if ts < minTS[sourceID]:
            minTS[sourceID] = ts
        temp = float(row[THERM1_TEMP_INDEX])
        if temp > maxTemperature[sourceID]:
            maxTemperature[sourceID] = temp
            maxTemperatureTS[sourceID] = ts
        if temp < minTemperature[sourceID]:
            minTemperature[sourceID] = temp
            minTemperatureTS[sourceID] = ts
        # insert new columns
        # calculate BEFORE adding column (changes indexes!)
        relative_altitude = float(row[ALTITUDE_FIELD]) - float(row[REF_ALTITUDE_FIELD])
        relative_altitude_str = '{:.1f}'.format(relative_altitude)
        if relative_altitude > maxRelativeAltitude[sourceID]:
            maxRelativeAltitude[sourceID] = relative_altitude
            maxRelativeAltitudeTS[sourceID] = ts
        if sourceID == 0:
            row.insert(TIMESTAMP_FIELD + 1, "")
            row.insert(REF_ALTITUDE_FIELD + 2, relative_altitude_str)
        else:
            row.insert(TIMESTAMP_FIELD + 1, "")
            row.insert(TIMESTAMP_FIELD + 2, "")
            row.insert(REF_ALTITUDE_FIELD + 3, relative_altitude_str)
        line = ', '.join(str(x) for x in row)
        outputFiles[sourceID].write(line + '\n')

print('Skipped', numSkippedRows, 'lines in input file (not records)')
print('Processed', numRecords, 'records')
check = numRecords
for i in [0, 1, 2]:
    print('  source', i, ':', numRecordsPerSource[i], 'records, from timestamp', minTS[i], 'to', maxTS[i])
    print("             max. relative altitude: {:.1f}m at timestamp {}".format(maxRelativeAltitude[i], maxRelativeAltitudeTS[i]))
    print("             max. temperature: {:.1f}°C at timestamp {}".format(maxTemperature[i], maxTemperatureTS[i]))
    print("             min. temperature: {:.1f}°C at timestamp {}".format(minTemperature[i], minTemperatureTS[i]))
    check -= numRecordsPerSource[i]
if check != 0:
    print(MYSELF + ': **** Inconsistency issue: numRecords is not sum of records from all sources.')
print(MYSELF + ': End of job.')

exit(0)
