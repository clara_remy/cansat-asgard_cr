# Parameter file for utility processRecordFile.py (Comma-separated values, CSV). 
# Complete nnnnn values, do not add or remove any line or column
#
# Input file (plain file from can or RF-Transceiver, records must by 14 columns)
GMiniTestData.csv
# Relevant data starts from (TS main can)
4283942
# Relevant data stops at (TS main can)
4460542
# Origin for relative timestamps (TS main can, will become ts 0)
4283942
# TS of records from main can and subcans assumed to be simultaneous (used to compute timestamp offsets)
# TS main can record
5497542
# TS subcan 1 record
5372656
# TS subcan 2 record)
5205288
# Offset for temperature thermistor 1 in main can
-0.1
# Offset for temperature thermistor 2 in main can
0.2
# Offset for temperature thermistor 1 in subcan 1
0.3
# Offset for temperature thermistor 2 in subcan 2
0.4
# Offset for temperature thermistor 1 in subcan 2
0.5
# Offset for temperature thermistor 2 in subcan 2
0.6




