const uint8_t thePinNumber=9;
const uint16_t shortCut=300;
const uint16_t longCut=2800; // delay mis pour faire les mesures
void setup() {
  // put your setup code here, to run once:
 pinMode (thePinNumber,OUTPUT);
}

void loop()
{
  // put your main code here, to run repeatedly:
  digitalWrite (thePinNumber, HIGH);
  delay (longCut);
  digitalWrite (thePinNumber, LOW);
  delay (shortCut);
}
