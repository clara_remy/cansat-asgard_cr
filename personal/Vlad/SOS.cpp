#include "SOS.h"
bool SOS::begin(uint8_t PinNbr) {
  LED_PinNbr = PinNbr;
  pinMode( LED_PinNbr, OUTPUT);
  return true;
}

void SOS::flashOnce(uint16_t duration) {

  digitalWrite (LED_PinNbr, HIGH);
  delay(duration);
  digitalWrite(LED_PinNbr, LOW);
  delay(duration);
}

void SOS::S () {

  for (int i = 0; i < 3; i++) {
    flashOnce (DurationS);
  }
}

void SOS::O () {
  for (int i = 0 ; i <3; i++) {
    flashOnce (DurationO);
  }
}

void SOS::flash () {
  S();
  O();
  S();
}
