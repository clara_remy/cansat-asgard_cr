#define DEBUG_CSPU // On emploit le préprocesseur
#include "DebugCSPU.h"
#define DBG 1

int a = 16;

void setup() {
  DINIT(112500);
  Serial << "Début du programme." << ENDL;
}

void loop() {
  Serial << "J'ai " << a << " ans." << ENDL;
  DPRINTLN(DBG, "Message du DPRINT");
  delay(2000);
}
