

#include "SOS.h"

bool SOS::begin(uint8_t pinNbr) {
  LED_PinNbr=pinNbr;
  pinMode (LED_PinNbr, OUTPUT);
  return true;
}

void SOS::flashOnce(uint16_t duration) { // fonction flash => 1 clignotement, durée à déterminer.
  digitalWrite (LED_PinNbr, HIGH);
  delay (duration);
  digitalWrite (LED_PinNbr, LOW);
  delay (duration);
}

void SOS::S () { // fonction lettre S => 3 courts fonctions flash.
  for (int i = 0; i < 3; i++) {
    flashOnce(DurationS);
    delay(500);
  }
}

void SOS::O () { // fonction lettre O => 3 longs fonctions flash.
  for (int i = 0; i < 3; i++) {
    flashOnce(DurationO);
    delay(500);
  }
}

void SOS::blink () {
  S();
  O();
  S();
}
