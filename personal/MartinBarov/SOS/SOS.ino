#include"SOS.h" // inclure SOS.h
SOS mySOS; // créer objet de class SOS.
const uint8_t LED_PinNbr = 10;
void setup () {
  Serial.begin(115200);
  delay(1000);
  mySOS.begin(LED_PinNbr);
}
void loop () {
  mySOS.blink();
  delay(3000);
}
