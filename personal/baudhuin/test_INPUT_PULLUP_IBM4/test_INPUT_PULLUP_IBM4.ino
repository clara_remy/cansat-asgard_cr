
#define CSPU_DEBUG
#include "CSPU_Debug.h"

constexpr byte InputPin=7;

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  pinMode(InputPin, INPUT_PULLUP);
}

void loop() {
  bool isHigh = digitalRead(InputPin);
  if (isHigh) Serial << "HIGH" << ENDL;
  else Serial << "LOW" << ENDL;
  delay(1000);
}
