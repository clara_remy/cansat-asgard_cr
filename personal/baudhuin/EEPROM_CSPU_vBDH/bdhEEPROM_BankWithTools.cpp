/*
 * This is a subclass of EEPROM_Bank completed with utility methods for testing only
 */

#include "EEPROM_BankWithTools.h"
#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#define DBG_READ_WRITE 0
#define DBG_ERASE 0
#define DBG_READ_DATA 0
#define DBG_DUMP_MEMORY 1
#define DBG_READER_AT_END 0

// ----------------- Constructor & overloaded methods ---------------

EEPROM_BankWithTools::EEPROM_BankWithTools(const unsigned int theMaintenancePeriodInSec)
	: EEPROM_Bank(theMaintenancePeriodInSec)
{
}


// --------------------- Utility methods -----------------------------

void EEPROM_BankWithTools::printMemory() const {
  char str[50];
  const EEPROM_Header header=getHeader();
  const Flags flags=getFlags();

  Serial << F("________________________________") << ENDL;
  Serial <<  F("Content of Memory:") << ENDL;
  printHeaderFromChip();
  Serial <<  F("Header in chip #") << (int) getFirstEEPROM_Chip() << ENDL;
  Serial <<  F("In-memory first free chip: ") << (int) header.firstFreeChip << ENDL;
  sprintf(str,"In-memory first free byte: 0x%02x" , header.firstFreeByte);
  Serial.println(str);
  Serial  << F("Flags: Initialized:") << flags.initialized
          << F(", headerToBeUpdated: ") << flags.headerToBeUpdated
          << F(", memoryFull:") << flags.memoryFull << ENDL;
  Serial << F("Space: total: ") << getTotalSize() << F(", free: ") << getFreeSpace() << F(" (") << getNumFreeRecords() << F(" rec.)") << ENDL;
  hexDumpData();
  Serial << F("--------------------------------") << ENDL;
}

void EEPROM_BankWithTools::printHeaderFromChip() const {
  EEPROM_Header h;
  const EEPROM_Header header=getHeader();
  const HardwareScanner* hardware=getHardware();

  h.headerKey = header.headerKey + 1;
  int chipNbr = 0;
  int numChips = hardware->getNumExternalEEPROM();
  while ((chipNbr < numChips) && (h.headerKey != header.headerKey)) {
    readFromEEPROM(hardware->getExternalEEPROM_I2C_Address(chipNbr), 0, (byte *) &h, sizeof(EEPROM_Header));
    chipNbr++;
  }
  if (h.headerKey == header.headerKey) {
    chipNbr--;
    char str[60];
    sprintf(str, "Header found in chip #%d at I2C 0x%02d (%d bytes).",
            chipNbr, (int) hardware->getExternalEEPROM_I2C_Address(chipNbr), (unsigned int) sizeof(h));
    Serial.println(str);
    sprintf(str, "   Key            : 0x%04x", h.headerKey);
    Serial.println(str);
    Serial <<  F("   NumChips       : ") << (int)  h.numChips << ENDL;
    sprintf(str, "   Last address   : %u (0x%04x)" , h.chipLastAddress , h.chipLastAddress);
    Serial.println(str);
    sprintf(str, "   Record size    : %d (0x%04x) bytes.", h.recordSize, h.recordSize);
    Serial.println(str);
    Serial <<  F("   First free chip: ") << (int) h.firstFreeChip<< ENDL;
    sprintf(str, "   First free byte: 0x%04x", h.firstFreeByte);
    Serial.println(str);
  }
  else Serial << F("No header found in chips") << ENDL;
}

void EEPROM_BankWithTools::hexDumpData() const {
  const unsigned int bufferSize = 8;
  byte buffer[bufferSize];
  const HardwareScanner* hardware=getHardware();
#ifdef USE_COUT
  cout << "--- Memory dump ---" << endl;
  cout << setfill('0') << setw(2) << hex;
#else
  DPRINTSLN(DBG_DUMP_MEMORY, "--- Memory dump ---");
#endif
  for (int chip = 0; chip < getHeader().numChips ; chip++) {
#ifdef USE_COUT
    cout << "chip #" << dec << chip << " -----------" << endl;
#else
    DPRINTS(DBG_DUMP_MEMORY, "Chip #");
    DPRINT(DBG_DUMP_MEMORY, chip);
    DPRINTSLN(DBG_DUMP_MEMORY, " -----------");
#endif
    byte I2C_Address = hardware->getExternalEEPROM_I2C_Address(chip);
    unsigned int lastAddress = hardware->getExternalEEPROM_LastAddress(chip);
    for (unsigned int address = 0 ; address <= lastAddress; address += bufferSize)
    {
      readFromEEPROM(I2C_Address, address, buffer, 8);
#ifdef USE_COUT
      cout << "0x" << setw(4) << hex << address  << ":" ;
#else
      DPRINTS(DBG_DUMP_MEMORY, "Ox");
      DPRINT(DBG_DUMP_MEMORY, address, HEX);
      DPRINTS(DBG_DUMP_MEMORY, ":");
#endif
      hexDumpBuffer(buffer, bufferSize);
    }
  }
}

void EEPROM_BankWithTools::hexDumpBuffer(const byte *buffer, const unsigned int bufferSize) const {
#ifdef USE_COUT
  for (int i = 0; i < bufferSize; i++) {
    if ((i % 4) == 0) cout << " ";
    cout << setfill('0') << setw(2) << hex << (int) (buffer[i]) << " ";
  }
  cout << endl;
#else
  for (unsigned int i = 0; i < bufferSize; i++) {
    if ((i % 4) == 0) {
      DPRINTS(DBG_DUMP_MEMORY, " ");
    }
    DPRINT(DBG_DUMP_MEMORY, (int) (buffer[i]), HEX);
    DPRINTS(DBG_DUMP_MEMORY, " ");
  }
  DPRINTSLN(DBG_DUMP_MEMORY, " ");
#endif
}



