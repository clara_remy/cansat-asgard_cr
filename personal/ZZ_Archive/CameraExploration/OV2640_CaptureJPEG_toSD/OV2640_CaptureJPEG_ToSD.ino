/*
 * CaptureJPEG_ToSD.ino 
 * 
 * This program is based on example ArduCAM_Mini_Capture2SD. It captures JPEG images every 2 secondes.
 * 1. Set the sensor to JPEG mode.
 * 2. Capture and buffer the image to FIFO every 5 seconds
 * 3. Store the image to Micro SD/TF card with JPEG format. file name is 1.jpg, 2.jpg etc. Files from previous run are 
 *    overwritten.
 *    
 * Different timestamps are collected to document performance of the capture and saving to file phases.    
 * 
 * See configuration parameters below.
 */
#include <ArduCAM.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include "memorysaver.h"

//This demo can only work on OV2640_MINI_2MP or OV5642_MINI_5MP or OV5642_MINI_5MP_BIT_ROTATION_FIXED platform.
#if !(defined OV5642_MINI_5MP || defined OV5642_MINI_5MP_BIT_ROTATION_FIXED || defined OV2640_MINI_2MP || defined OV3640_MINI_3MP)
#error Please select the hardware platform and camera module in the ../libraries/ArduCAM/memorysaver.h file
#endif

// ******************************* CONFIGURATION SETTINGS *********************************
/* Valid JPEG size values: 
 *  OV2640_160x120, 176x144, 320x240, 352x288, 640x480, 800x600,  
 *  1024X768, 1280x1024, 1600x1200 
 */
const int myJPEG_Size=OV2640_1600x1200 ;

#define USE_HW_SCANNER                // Define for a complete exploration of hardware during setup. 
//#define DETAILED_PROGRESS_ON_SERIAL // Define for extensive progress information (makes timing measurements irrelevant)
#define SD_CS 9                       // SD chip-select pin
#define SPI_CS 7                      // ArduCAM chip-select pin.


// ********************************** END CONFIGURATION ***********************************

#if defined (OV2640_MINI_2MP)
ArduCAM myCAM( OV2640, SPI_CS );
#elif defined (OV3640_MINI_3MP)
ArduCAM myCAM( OV3640, SPI_CS );
#else
ArduCAM myCAM( OV5642, SPI_CS );
#endif

#include "SerialStream.h" 
#include "HardwareScanner.h"

void myCAMSaveToSDFile() {
  char str[8];
  byte buf[256];
  static int i = 0;
  static int k = 0;
  uint8_t temp = 0, temp_last = 0;
  uint32_t length = 0;
  bool is_header = false;
  File outFile;
  //Flush the FIFO
  myCAM.flush_fifo();
  //Clear the capture done flag
  myCAM.clear_fifo_flag();
  
  //Start capture
  Serial << ENDL << F("start Capture") << ENDL;
  unsigned long startTS=millis();
  myCAM.start_capture();
  while (!myCAM.get_bit(ARDUCHIP_TRIG , CAP_DONE_MASK)) { 
    delay(5);  // Added this delay in an attempt to remove the capture in 0 msec which are always corrupt.
    }
  unsigned long endCaptureTS=millis();
  unsigned long endWriteTS;
#ifdef DETAILED_PROGRESS_ON_SERIAL
  Serial << F("Capture Done in ") << endCaptureTS-startTS << F(" msec") << ENDL;
#endif

  
  length = myCAM.read_fifo_length();
  uint32_t savedFifoLength=length;
#ifdef DETAILED_PROGRESS_ON_SERIAL
  Serial.print(F("The fifo length is :"));
  Serial.println(length, DEC);
#endif
  if (length >= MAX_FIFO_SIZE) //384K
  {
    Serial.println(F("Over size (aborted)."));
    return ;
  }
  if (length == 0 ) //0 kb
  {
    Serial.println(F("Size is 0 (aborted)."));
    return ;
  }
  
  //Construct a file name
  k = k + 1;
  itoa(k, str, 10);
  strcat(str, ".jpg");
  //Open the new file
  outFile = SD.open(str, O_WRITE | O_CREAT | O_TRUNC);
  if (!outFile) {
    Serial.print(F("File open faild for "));
    Serial.print(str);
    Serial.println(F(" (aborted)."));
    return;
  }
#ifdef DETAILED_PROGRESS_ON_SERIAL
  else {
    Serial.print(F("Opened file "));
    Serial.println(str);
  }
#endif
  myCAM.CS_LOW();   // select ArduCAM
  myCAM.set_fifo_burst();  // Enter burst read mode: every time we'll transfer 0x00, we'll get
                           // 1 byte of data. First one is a dummy byte.
                           // 0xFF followed by 0xD9 is the end of file flag
                           // 0xFF followed by 0xD8 is the begining of the header.
                           // ??? seems that there is nothing else than the header ???
  bool headerEncountered=false;
  while ( length-- )
  {
    temp_last = temp;
    temp =  SPI.transfer(0x00);
    // Serial << F("  Length=") << length << ": " << F("read byte: ");
    // Serial.println(temp, HEX);
    //Read JPEG data from FIFO
    if ( (temp == 0xD9) && (temp_last == 0xFF) ) //If find the end ,break while,
    {
#ifdef DETAILED_PROGRESS_ON_SERIAL
      Serial << F("End reached") << ENDL;
#endif
      buf[i++] = temp;  //save the last  0XD9
      //Write the remain bytes in the buffer
      myCAM.CS_HIGH();
      outFile.write(buf, i);
      //Close the file
      outFile.close();
      endWriteTS=millis();
      Serial << F("Image save OK: ")<<  str << F(", ") << savedFifoLength << F(" bytes.") << ENDL;
      is_header = false;
      i = 0;
    }
    if (is_header == true)
    {
      //Write image data to buffer if not full
      if (i < 256) 
        buf[i++] = temp;
      else
      {
        //Write 256 bytes image data to file
#ifdef DETAILED_PROGRESS_ON_SERIAL        
        Serial << F("Writing 256 bytes to file...");
#endif
        myCAM.CS_HIGH();
        int written = outFile.write(buf, 256);
        if (written != 256) {
          Serial << F("Error: only wrote :") << written << F(" bytes") << ENDL;
        } else {
#ifdef DETAILED_PROGRESS_ON_SERIAL
          Serial << F("OK") << ENDL;
#endif
        }
        i = 0;
        buf[i++] = temp;
        myCAM.CS_LOW();
        myCAM.set_fifo_burst();
      }
    }
    else if ((temp == 0xD8) & (temp_last == 0xFF))
    {
#ifdef DETAILED_PROGRESS_ON_SERIAL
      Serial << F("Entering header...") << ENDL;
#endif
      is_header = true;
      headerEncountered=true;
      buf[i++] = temp_last;
      buf[i++] = temp;
    }
  } // while
  if (!headerEncountered) {
    Serial << F("*** Error: never got the start of file OxFF - OxD9") << ENDL;
    myCAM.CS_HIGH(); // Make sure we release the bus.
    //Close the file and remove (empty)
    outFile.close();
    SD.remove(str);
    delay(2000);
  } else  if (is_header) {
    Serial << F("****Error: got the whole fifo, but no 0xFF - 0xD8 as end of file marker") << ENDL;
    myCAM.CS_HIGH(); // Make sure we release the bus.
    //Close the file (probably corrupt? ). 
    outFile.close();
    delay(2000);
  } else {
    Serial << "Capture= " << endCaptureTS-startTS << " msec" << ENDL;
    Serial << "Write to SD= " << endWriteTS-endCaptureTS << " msec" << ENDL;
    Serial << "Total= " << endWriteTS-startTS << " msec" << ENDL;
  }
}

//------------------------------------------------------------------------------

void setup() {
  uint8_t vid, pid;
  uint8_t temp;
  Serial.begin(115200);
  Serial << "Setup: Serial OK" << ENDL;
#ifdef USE_HW_SCANNER
  HardwareScanner hw;
  hw.init();
  hw.printFullDiagnostic(Serial);
#else
  Wire.begin(); // This call is included in the HardwareScanner.
#endif

  //set the CS as an output: Probably useless since included in SPI.begin()
 // pinMode(SPI_CS, OUTPUT);
  // initialize SPI:
  SPI.begin();  // This call is not included in the HardwareScanner.

  //Initialize SD Card
  while (!SD.begin(SD_CS)) {
    Serial.println(F("SD Card Error!")); delay(1000);
  }
  Serial.println(F("SD Card detected."));

  Serial.println(F("ArduCAM Start!"));

  while (1) { 
    //Check if the ArduCAM SPI bus is OK
    myCAM.write_reg(ARDUCHIP_TEST1, 0x55);
    Serial << "Wrote 0x55... Reading again: ";
    Serial.flush();
    temp = myCAM.read_reg(ARDUCHIP_TEST1);
    Serial << "Ox";
    Serial.println(temp, HEX);

    if (temp != 0x55) {
      Serial.println(F("SPI interface Error!"));
      delay(1000); 
      continue;
    } else {
      Serial.println(F("SPI interface OK.")); 
      break;
    }
  }
 
#if defined (OV2640_MINI_2MP)
  while (1) {
    //Check if the camera module type is OV2640
    myCAM.wrSensorReg8_8(0xff, 0x01);
    myCAM.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
    myCAM.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
    if ((vid != 0x26 ) && (( pid != 0x41 ) || ( pid != 0x42 ))) {
      Serial.println(F("Can't find OV2640 module!"));
      delay(1000); continue;
    }
    else {
      Serial.println(F("OV2640 detected.")); break;
    }
  }
#elif defined (OV3640_MINI_3MP)
  while (1) {
    //Check if the camera module type is OV3640
    myCAM.rdSensorReg16_8(OV3640_CHIPID_HIGH, &vid);
    myCAM.rdSensorReg16_8(OV3640_CHIPID_LOW, &pid);
    if ((vid != 0x36) || (pid != 0x4C)) {
      Serial.println(F("Can't find OV3640 module!"));
      delay(1000); continue;
    } else {
      Serial.println(F("OV3640 detected.")); break;
    }
  }
#else
  while (1) {
    //Check if the camera module type is OV5642
    myCAM.wrSensorReg16_8(0xff, 0x01);
    myCAM.rdSensorReg16_8(OV5642_CHIPID_HIGH, &vid);
    myCAM.rdSensorReg16_8(OV5642_CHIPID_LOW, &pid);
    if ((vid != 0x56) || (pid != 0x42)) {
      Serial.println(F("Can't find OV5642 module!"));
      delay(1000); continue;
    }
    else {
      Serial.println(F("OV5642 detected.")); break;
    }
  }
#endif
  myCAM.set_format(JPEG); // other option: BMP (see other test sketch).
  myCAM.InitCAM();   // Warning: call AFTER set_format. 
#if defined (OV2640_MINI_2MP)
  myCAM.OV2640_set_JPEG_size(myJPEG_Size);
#elif defined (OV3640_MINI_3MP)
  myCAM.OV3640_set_JPEG_size(OV3640_320x240);
#else
  myCAM.write_reg(ARDUCHIP_TIM, VSYNC_LEVEL_MASK);   //VSYNC is active HIGH
  myCAM.OV5642_set_JPEG_size(OV5642_320x240);
#endif
  delay(1000);
  Serial.println(F("Setup done."));
}

//=================================================================================
void loop() {
  myCAMSaveToSDFile();
  delay(2000);
}
