/** Strategy for the blinking LED of the RF Transceiver

*/
#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1

const int LED = 13;
unsigned long previousTime = 0;
const unsigned long number = 5000;
const int blinkDelay = 1000;
bool doWhat = true;
unsigned long previousBlinkTime = 0;
const char* stringReceived != NULL;

void doSomething() {
  ;
}

void blink(const int blinkDelay) {
  if (millis() - previousBlinkTime >= blinkDelay) {
    digitalWrite(LED, !digitalRead(LED));
    previousBlinkTime = millis();
  }
}

void Led(bool doWhat, const int blinkDelay) {
  if (doWhat == false) {
    digitalWrite(LED, LOW);
  } else {
    blink(blinkDelay);
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  bool state = HIGH;
  previousBlinkTime = millis();
}

void loop() {
  if (stringReceived) {
    previousTime = millis();
    doSomething();
  }

  if ((millis() - previousTime) >= number) {
    doWhat = false;
    Led(doWhat, blinkDelay);
  } else {
    doWhat = true;
    Led(doWhat, blinkDelay);
  }
}
