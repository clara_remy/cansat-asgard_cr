// rf22_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing client
// with the RF22 class. RF22 class does not provide for addressing or reliability.
// It is designed to work with the other example rf22_server

#include <SPI.h>
#include <RH_RF22.h>
#include "Arduino.h"

// Singleton instance of the radio
RH_RF22 rf22(5, 10);

int readings = 0;

void setup() 
{
  Serial.begin(9600);
  while(!Serial);
  Serial.println("START");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  if(!rf22.init()){
    Serial.println("RF22 init failed");
    while(!rf22.init());
  }
  else{
    Serial.println("it's ok");
  }
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
}

void loop()
{
  rf22.setModeRx();
  while (1)
  {
    Serial.println("Sending to rf22_server");
    digitalWrite(LED_BUILTIN, HIGH);
    // Send a message to rf22_server
    uint8_t data[] = "Hello World!";
    rf22.send(data, sizeof(data));
   
    rf22.waitPacketSent();
    // Now wait for a reply
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    uint8_t buf[RH_RF22_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);

    if (rf22.waitAvailableTimeout(500))
    { 
      // Should be a message for us now 
      readings = rf22.rssiRead();  
      if (rf22.recv(buf, &len))
      {
        Serial.print("got reply: ");
        
        Serial.println((char*)buf);
        Serial.print("rssi : ");
        Serial.println(readings);
      }
      else
      {
        Serial.println("recv failed");
      }
    }
    else
    {
      Serial.println("No reply, is rf22_server running?");
    }
  }
}
