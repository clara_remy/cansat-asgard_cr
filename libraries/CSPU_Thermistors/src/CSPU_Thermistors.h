/*
 * CSPU_Thermistors.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup CSPU_Thermistors
 * in the class documentation block.
 */

 /** @defgroup CSPU_Thermistors CSPU_Thermistors library
 *  @brief The library of classes used to interface thermistors. *  
 *  The CSPU_Thermistors library contains various thermistor-related classes without any specific link to a particular project:
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - CSPU_Debug
 *  - CSPU_CansatCore
 *  - elapsedMillis
 *  
 *  
 *  _History_\n
 *  The library was created by in Sept. 2021, while restructuring the general cansat libraries 
 *  for maintainability.
 */

