/*
 * CSPU_CansatData.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup CSPU_CansatConfig
 * in the class documentation block.
 */

 /** @defgroup CSPU_CansatConfig CSPU_CansatConfig library
 *  @brief The library of core data classes and definition headers, relevant to any Cansat project.
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - CSPU_Debug
 *  
 *  
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS) and further enriched
 *  during the next projects.
 *  The library was split in several dedicated libraries in Sept. 2021.
 */

