/*
 * DotStar.h
 */

#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

/** A class for managing DotStar LEDs. It currently only supports the onboard
 *  DotStar LED of an ItsyBitsy M4 board.
 *  Understanding communication with DotStar:
 *  https://cpldcpu.wordpress.com/2014/11/30/understanding-the-apa102-superled/
 */
class DotStar {
public:
	/** Shutdown the on-board DotStar LED. This method bit bangs the command without
	 *  using a Serco or UART.
	 */
	static void shutdownOnBoard();

	/** Bit bang one or more identical bits on the data pin.
	 *  Values are valid on the rising edge of the clock pin.
	 *  @param dataPin The digital output pin for the data. It is expected to be configured.
	 *  @param clockPin The digital output pin for the clock. It is expected to be configured.
	 *  @param bitValue The value of the bit(s) to be sent.
	 *  @param numRepeats The number of times the bit must be sent.
	 */
	static void sendBit(uint8_t dataPin, uint8_t clockPin, bool bitValue, uint16_t numRepeats=1);
};

