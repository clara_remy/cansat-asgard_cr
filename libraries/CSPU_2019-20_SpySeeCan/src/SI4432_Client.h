/*
   SI4432Client.h
*/

#pragma once
#include <Arduino.h>
#include "RH_RF22.h"
typedef int8_t powerDBm_t;

/**
   @ingroup SpySeeCanCSPU
   @brief This class takes care of the interactions with the SI4432 module.
   It currently only provides reading of the RSSI (Received Signal Strength Intensity), averaged on
   several readings and converted (in dBm).
*/
class SI4432_Client
{

public:
   /**
      @param theSlaveSelectPin the Arduino pin number of the output to use to select the module before accessing it
    */
   SI4432_Client(uint8_t theSlaveSelectPin);
   /**
      @param theSlaveSelectPin the Arduino pin number of the output to use to select the module before accessing it
      @param theInterruptPin The interrupt Pin number that is connected to the module NIRQ interrupt line
    */
   SI4432_Client(uint8_t theSlaveSelectPin, uint8_t theInterruptPin);

   /**
        @brief Initialize the SI4432 before use.
        @param minFreq The minimum frequency
        @param maxFreq The maximum frequency
        @param stepFreq The step of frequency between each read
        @return True if initialization was successful, false otherwise.
    */
   bool begin(float minFreq = 0.0, float maxFreq = 0.0, float stepFreq = 0.0);

   /**
       @brief Read the power on a frequency given
       @param frequencyMHz the frequency to scan in MHz
       @param correc set True if you want to correct the power
       @return The power read in dBm. If frequency is outside the allowed range, value is -127.
    */
   powerDBm_t readPower(float frequencyMHz /* REMOVED ,bool correc = false */);

#ifdef REMOVED
   /**
       @brief Read the power on diffrent frequencies
       @param power the table to contain the power
       @param nbData the number of data of the table @p power
       @param correc set True if you want to correct the power
       @param minFreq the starting frequency of the scan
       @param maxFreq the maximum frequency to scan
       @param stepFreq the step of frequency between each read
    */
   bool readPower(powerDBm_t power[], byte nbData, bool correc = false, float minFreq = 0.0, float maxFreq = 0.0, float stepFreq = 0.0);

   float getMinAttFrequency() const { return minFrequency; }
   float getMaxAttFrequency() const { return maxFrequency; }
   byte getNumStepFreq() const { return 0; }
   const byte *const getAttenuationOffset() const { return NULL; } ///A continuer
   byte getAttenuationSize() const { return 0; }
   float getAttStepFrequency() const { return stepFrequency; }
#endif
   static constexpr float minSupportedFrequency = 433.0; /**< The minimum frequency of the SI4432*/
   static constexpr float maxSupportedFrequency = 868.0; /**< The maximum frequency of the SI4432*/
   static constexpr float defaultStepFrequency = 100.0;  /**< The step of frequency between each read*/

private:
   RH_RF22 rf22;
#ifdef REMOVED
   float minFrequency;
   float maxFrequency;
   float stepFrequency;
#endif

   /** @brief Converts the rssi values in dBm.
        @param rssi The rssi value
        @return The converted value
    */
   powerDBm_t dBmConversion(uint8_t rssi);
// REMOVED    powerDBm_t correctPowerForAntennaAttenuation(powerDBm_t power, float freq);
};
