/**
    test_CansatXBeeClient-SleepMode.ino

  Test the sleep mode of XBee driven by the Sleep Request pin.
  This program checks the XBee actually goes to sleep mode, and tests the minimum delay
  for the module to wake-up until is can emit a message.

  Wiring: µC to XBee module (can-side).
   3.3V to VCC
   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
   GND  to GND
   SLEEP_RQ (pin 9) to pin pin_XBeeSleepRequest (defined in CansatConfig.h).
   ON/SLEEP (pin 13) to pin pin_XBeeOnSleep (defined in CansatConfig.h).

   The XBee module must properly be configured as an End-Device, with pin sleep
   mode enabled, i.e. CE=0, SM=1, D8=1, D9=1

   This program emits CansatRecords (or any subclass of it according to symbol RECORD_CLASS)
   to the ground (using address defined in Cansatconfig.h.
   Use the RF_Transceiver program to display the records received and their timestamps
   (be sure to use the version apprioriate for the kind of records emitted.

   For testing, the sleep mode can be assessed by testing pin 13 (ON_SLEEP), which should be LOW
   when sleeping.

*/
#define RECORD_CLASS GMiniRecord  // valid values: CansatRecord or any of its subclasses.

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards"
#endif

#define DBG 1
#define Q(x) #x
#define QUOTE(x) Q(x)

#ifdef FILE_ARG
#include QUOTE(FILE_ARG)
#endif

#define RECORD_CLASS_HEADER RECORD_CLASS.h

#include "CansatConfig.h"
#include "CansatXBeeClient.h"
#include QUOTE(RECORD_CLASS_HEADER)
#include "CSPU_Test.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
RECORD_CLASS myRecord;
uint16_t numErrors = 0;

class TestXBeeClient {
  public:
    static bool autoWakeUpXBeeModule(XBeeClient& xb) {
      return xb.autoWakeUpXBeeModule();
    };
    static bool autoPutXBeeModuleToSleep(XBeeClient& xb, bool wait = false) {
      return xb.autoPutXBeeModuleToSleep(wait);
    };
};

void testSleepModeEntry() {
  Serial << ENDL << ENDL << "Checking the module is in sleep mode and not responding anymore... " << ENDL;
  xbc.wakeUpXBeeModule();
  delay(100);  // Just to control the initial condition: module up and awake.
  if (!xbc.isModuleOnline()) {
    Serial << "*** Error: Module is not online at start of testSleepModeEntry() test" << ENDL;
    numErrors++;
    return;
  }

  // Let's start the actual test.
  xbc.putXBeeModuleToSleep(true);
  if (!xbc.isModuleOnline()) {
    Serial << "Module is sleeping: OK" << ENDL;
  } else {
    Serial << "**** Error: The XBee module is not sleeping" << ENDL;
    numErrors++;
    return;
  }

  Serial << ENDL << ENDL << "Exiting sleep mode and checking the module is responding again... " << ENDL;
  xbc.wakeUpXBeeModule();
  if (xbc.isModuleOnline()) {
    Serial << "Module is awake." << ENDL;
  } else {
    Serial << "**** Error: The can is still sleeping" << ENDL;
    numErrors++;
    return;
  }
  Serial << "Errors so far: " << numErrors << ENDL;
}


void testEmissionWithSleepMode(uint16_t periodInMsec, bool putToSleep, bool askConfirmation) {
  Serial << "Sending records every " << periodInMsec << " msec during 10 seconds" << ENDL;
  Serial << "Listen with 0000-00_templates/RF-Transceiver" << ENDL;

  if (putToSleep) {
    xbc.putXBeeModuleToSleep(true);
    if (xbc.isModuleOnline()) {
      Serial << "*** Error: Module is online at start of testEmissionWithSleepMode() after putToSleep()" << ENDL;
      numErrors++;
      return;
    }
  } else{
    xbc.wakeUpXBeeModule();
    if (!xbc.isModuleOnline()) {
      Serial << "*** Error: Module is NOT online at start of testEmissionWithSleepMode() after wakeUp(). Waiting more..." << ENDL;
      numErrors++;
      uint8_t counter=0;
      while (counter < 10 && !xbc.isModuleOnline()) {
        Serial << "Still not online..." << ENDL;
        counter++;
      }
      return;
    }
  }

  for (int i = 1; i <= 10000 / periodInMsec; i++) {
    Serial << i << ": ts=" << myRecord.timestamp << "..." << ENDL;
    xbc.send(myRecord);
    myRecord.timestamp += periodInMsec; //
    delay(periodInMsec);
  }
  if (askConfirmation && (!CSPU_Test::askYN("Did you receive records with the right period ?")))
  {
    numErrors++;
  }
}

void checkSM_Value(uint8_t expected) {
  uint8_t SM_Value;
  if (!xbc.queryParameter("SM", SM_Value)) {
    Serial << "*** Error checking XBee SM parameter" << ENDL;
    numErrors++;
    return;
  }
  if (SM_Value != expected) {
    Serial << "*** Error: SM should be " << expected << ", got " << SM_Value << ENDL;
    numErrors++;
  }
}

void testSleepWakePairs() {
  Serial << "Testing sleep/wake pairs..." << ENDL;
  Serial << "  Unconfiguring sleep mode (pin connected, sleep inactive)..." << ENDL;
  xbc.configureSleepMngtPins(pin_XBeeSleepRequest, pin_XBeeOnSleep, false);
  if (xbc.putXBeeModuleToSleep()) {
    Serial << "*** Error: module should not go to sleep (sleep disabled)" << ENDL;
    numErrors++;
  } else {
    Serial << "Test ok" << ENDL;
  }
  Serial << "  Unconfiguring sleep mode (pin disconnected, =0)..." << ENDL; 
  xbc.configureSleepMngtPins(0, pin_XBeeOnSleep);
  if (xbc.putXBeeModuleToSleep()) {
    Serial << "*** Error: module should not go to sleep (pin = 0)" << ENDL;
    numErrors++;
  } else {
    Serial << "Test ok" << ENDL;
  }
  checkSM_Value(0);
  bool testOK=true;
  for (int i = 0; i < 5; i++) {
    if (TestXBeeClient::autoWakeUpXBeeModule(xbc)) {
      Serial << "*** Error: autoWakeUp should not have effect (sleep disable)" << ENDL;
      testOK = false;
    }
  }
  for (int i = 0; i < 5; i++) {
    if (TestXBeeClient::autoPutXBeeModuleToSleep(xbc)) {
      Serial << "*** Error: autoSleep should not have effect (sleep disable)" << ENDL;
      testOK=false;
    }
  }
  if(testOK) {
    Serial << "Test ok" << ENDL;
  }

  Serial << "  Reconfiguring sleep mode..." << ENDL;
  xbc.configureSleepMngtPins(pin_XBeeSleepRequest, pin_XBeeOnSleep, true);
  checkSM_Value(1);
  if (!xbc.putXBeeModuleToSleep()) {
    Serial << "*** Error: module should go to sleep" << ENDL;
    numErrors++;
  } else {
    Serial << "Test ok" << ENDL;
  }
  if (xbc.putXBeeModuleToSleep()) {
    Serial << "*** Error: module should not go to sleep twice" << ENDL;
    numErrors++;
  } else {
    Serial << "Test ok" << ENDL;
  }
  if (!xbc.wakeUpXBeeModule()) {
    Serial << "*** Error: module should wake up" << ENDL;
    numErrors++;
  } else {
    Serial << "Test ok" << ENDL;
  }
  if (xbc.wakeUpXBeeModule()) {
    Serial << "*** Error: module should not wake up twice" << ENDL;
    numErrors++;
  } else {
    Serial << "Test ok" << ENDL;
  }

  //-----
  if (!xbc.isModuleOnline()) {
    Serial << "Error: module should not be sleeping..."  << ENDL;
  }else {
    Serial << "Test ok" << ENDL;
  }
  testOK=true;
  for (int i = 0; i < 5; i++) {
    if (TestXBeeClient::autoWakeUpXBeeModule(xbc)) {
      Serial << "*** Error: autoWakeUp should not have any effect on awake module" << ENDL;
      testOK=false;
    }
  }
  for (int i = 0; i < 5; i++) {
    if (TestXBeeClient::autoPutXBeeModuleToSleep(xbc)) {
      Serial << "*** Error: autoSleep should not have any effect on awake module" << ENDL;
      testOK=false;
    }
  }
  if (testOK) {
    Serial << "Test ok" << ENDL;
  }
  Serial << "  Putting XBee to sleep..." << ENDL;
  xbc.putXBeeModuleToSleep();
  if (!TestXBeeClient::autoWakeUpXBeeModule(xbc)) {
    Serial << "*** Error: autoWakeUp should have effect (first time)" << ENDL;
  }
  for (int i = 0; i < 5; i++) {
    if (TestXBeeClient::autoWakeUpXBeeModule(xbc)) {
      Serial << "*** Error: autoWakeUp should not have any effect on awake module" << ENDL;
    }
  }
  for (int i = 0; i < 5; i++) {
    if (TestXBeeClient::autoPutXBeeModuleToSleep(xbc)) {
      Serial << "*** Error: autoSleep should not have any effect on awake module the first 5 times" << ENDL;
    }
  }
  if (!TestXBeeClient::autoPutXBeeModuleToSleep(xbc)) {
    Serial << "*** Error: autoSleep should have effect on awake module the last time" << ENDL;
  }
  Serial << "XBee now sleeping" << ENDL;
  Serial << "  Errors so far: " << numErrors << ENDL;
}

// That pin #pin takes value value
void checkInput(uint8_t pin, bool value) {
  elapsedMillis elapsed=0;
  bool success = false;
  do {
    success = (digitalRead(pin) == value);
    if (!success) delay(1);
  } while ((elapsed<3000) && (!success));
  if (!success) {
    Serial << "*** ERROR: pin #" << pin << " still not " << (value ? "HIGH" : "LOW") << " after 3 seconds" << ENDL;
    numErrors++;
  } else {
    Serial << "pin #" << pin << " detected " << (value ? "HIGH" : "LOW") << " after " << elapsed << " msec" << ENDL;
  }
}

void testBareSignals() {
  // The XBee is assumed to be awake, with SM=1.
  checkInput(pin_XBeeOnSleep, HIGH);

  digitalWrite(pin_XBeeSleepRequest, HIGH);
  checkInput(pin_XBeeOnSleep, LOW);

  digitalWrite(pin_XBeeSleepRequest, LOW);
  checkInput(pin_XBeeOnSleep, HIGH);
  
}

void setup() {
  DINIT(115200);

  digitalWrite(LED_BUILTIN, HIGH);

  Serial << ENDL << ENDL;
  Serial << "***** Testing XBee sleep-mode *****" << ENDL;
  Serial << "Initialising Serials and communications..." << ENDL;
  if (pin_XBeeSleepRequest == 0) {
    Serial << "SLEEP_RQ pin (#9) not configured (pin_XBeeSleepRequest constant, in CansatConfig.h"  << ENDL;
    Serial << "Check configuration and connexions: SLEEP_RQ pin must be connected to µC" << ENDL;
    Serial << "Aborted." << ENDL;
    exit(-1);
  }
  RF.begin(115200);
  xbc.begin(RF); // no return value
  xbc.configureSleepMngtPins(pin_XBeeSleepRequest, pin_XBeeOnSleep, true);

  Serial << "Configuration of XBee module (assumed to be the CAN side of set " << RF_XBEE_MODULES_SET << ")" << ENDL;
  if (!xbc.printConfigurationSummary(Serial)) {
    Serial << "Error printing configuration summaruy. Is the XBee connected?" << ENDL;
    Serial << "Aborted" << ENDL;
    exit(-1);
  }

  Serial << "  Destination (ground): SH=0x";
  Serial.print(GroundXBeeAddressSH, HEX);
  Serial  << ", SL=0x";
  Serial.println(GroundXBeeAddressSL, HEX);

  myRecord.clear();
  Serial << "Initialisation over" << ENDL;

  testBareSignals();
  testSleepWakePairs();
  testSleepModeEntry();
  testEmissionWithSleepMode(1000, true, false);
  testEmissionWithSleepMode(100, true, false);
  testEmissionWithSleepMode(90, true, true);
  Serial << "For a period of 80 msec or less, we loose some records, but this is not caused by the sleep mode! " << ENDL;
  Serial << "Proof: now using shorter periods without using the sleep mode." << ENDL;
  Serial << "Current hypothesis: this is due to the fact that the  XBee module is an end-device rather than a router" << ENDL;
  Serial << "TO BE FURTHER INVESTIGATED" << ENDL;
  CSPU_Test::pressAnyKey();

  xbc.wakeUpXBeeModule();
  testEmissionWithSleepMode(80, false, false);
  testEmissionWithSleepMode(70, false, true);
  xbc.putXBeeModuleToSleep();


  Serial << "End of job. Number of errors:" << numErrors << ENDL;
}

void loop() {

}
