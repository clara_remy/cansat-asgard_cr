#include "Arduino.h"

#include "HardwareScanner.h" 
#include "TemperatureClient.h"
#include "IsaTwoRecord.h"

#define THERMISTOR_PIN A0

TemperatureClient temperature_client(THERMISTOR_PIN);

IsaTwoRecord record;
HardwareScanner hw;

unsigned long ts_temperature;					/**< Time used in the main loop */
const unsigned long timer_temperature = 1000; 	/**< Delay in ms between two temperature measurements */

void setup()
{
    delay(5000);
    Serial.begin(9600);
    Serial.println("Serial OK");
    hw.init(1,2);
    Serial.print("hw.getDefaultReferenceVoltage(): ");
    Serial.println(hw.getDefaultReferenceVoltage());
    Serial.print("hw.getNumADC_Steps(): ");
    Serial.println(hw.getNumADC_Steps());
    temperature_client.begin(hw.getDefaultReferenceVoltage(), hw.getNumADC_Steps());
    delay(500);
    

	ts_temperature = millis();
}

void loop()
{
	if (millis() - ts_temperature > timer_temperature) {
		ts_temperature = millis();

    if (temperature_client.readData(record)) {
		  Serial.println(record.temperatureThermistor);
    }
    else {
      Serial.println("Error in temperature_client.readData");
    }
	}
}
