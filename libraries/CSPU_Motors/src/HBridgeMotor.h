/*
   HBridgeMotor.h
*/

#pragma once

#include "Arduino.h"

/** @ingroup CSPU_Motors
    @brief Implementation of a Latch based on a worm screw and a motor power using a H-bridge.
    The class has been tested with:
    - Motor TO BE COMPLETED
    - H-Bridge L923D.
*/

class HBridgeMotor {

  public :
    /** Method to call before any other method. Initializes whatever must be.
        @param pwmPin the PWM Pin number actives the H-Bridge.
        @param forwardPin the Forward Pin is used to turn forward.
        @param reversePin the Reverse Pin is used to turn backwards.
        @param power the Power is the velocity of the motor (between 0 and 255).
        @param delayLock the Delay Lock is used to lock.
        @param delayUnlock the Delay Unlock is used to unlock.
        @return true if motor initialisation succeeded
    */
    bool begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin);
    void TurnForward (uint32_t delayForward, uint8_t power); /**< Method to turn the motor forward during a defined time of milliseconds */
    void TurnBackwards (uint32_t delayBackwards, uint8_t power); /**< Method to turn the motor backwards during a defined time of milliseconds */

  private :
  
    uint8_t HBridgeMotorPWM_Pin; /**< Number of the PWM pin for the ScrewLatch (Enable1 pin of H-Bridge) */
    uint8_t HBridgeMotorForward_Pin; /**< Number of the pin connected to pin #2 (input1) of the H-bridge */
    uint8_t HBridgeMotorReverse_Pin; /**< Number of the pin connected to pin #7 (input2) of the H-bridge */

};
